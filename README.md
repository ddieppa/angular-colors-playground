# Angular Colors Playground

This is a beginners project to start playing with [Angular](https://angular.io/)

1. Download the project as a [zip file](https://gitlab.com/ddieppa/angular-colors-playground/-/archive/master/angular-colors-playground-master.zip) clicking here or going to the right up corner of the repo and click on the cloud icon 
2. Once downloaded, unzip the file, `cd` into the unzipped folder, open a `cmd` and run `npm install` to install all the project dependencies
3. Then run `npm start` and navigate to `http://localhost:4200/`

Happy Coding :)


